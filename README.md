
<!-- README.md is generated from README.Rmd. Please edit that file -->
Captación de la población beneficiaria de TUS y AFAM en ECH
===========================================================

<!-- badges: start -->
<!-- badges: end -->
Este repositorio contiene las funciones de captación de la población beneficiaria de los programas Asignaciones Familiares (AFAM) y Tarjeta Uruguay Social (TUS) en la Encuesta Continua de Hogares (ECH) de Uruguay.

Para usar estas funciones es necesario tener instalados una serie de paquetes, el siguiente código te permite instalarlos. Esto se realiza por una única vez.

``` r
pkgs <- c("haven", "srvyr", "dplyr", "magrittr", "rlang")
install.packages(pkgs)
```

También debes tener descargados los microdatos de la ECH con la que vayas a trabajar. Una forma fácil de bajarlos de la web del INE es usando el paquete ech:

``` r
# install.packages("ech")
library(ech)

ech19 <- get_microdata(year = "2019", # Año/s a descargar (2011-2019)
                       folder = tempdir(), # Carpeta para descarga   
                       toR = TRUE) # Lo guarda en formato RData 
```

Modo de uso
===========

Las funciones consumen datos que están alojados en este respositorio, son los 3 archivos csv que también se deben descargar y colocar en la misma carpeta que las funciones.

Para usar las funciones de captación debemos cargarlas usando la función `source()`. Ejemplificamos el uso de `captacion_afam_ech()` y `captacion_tus_ech()` para la ECH de 2019

``` r
source("captacion_afam_ech.R")
source("captacion_tus_ech.R")             

tabla_afam <- captacion_afam_ech(data = ech19,  year = 2019) 
tabla_afam

tabla_tus <- captacion_tus_ech(data = ech19,  year = 2019) 
tabla_tus
```

Para otros años de la ECH, para la función captacion\_tus\_ech es necesario usar más parámetros de la función dado que las variables en la encuesta tienen nombres diferentes. Por ejemplo para 2014:

``` r
tabla_tus <- captacion_tus_ech(data = ech14,  year = 2014, e560_1 = "h157",  e560_1_1 = "h157_1") 
tabla_tus
```

Desarrollo
==========

``` r
Type: Functions
Title: Estimating the TUS and AFAM-PE population 
Version: 1.0
Authors@R:  c(person("Gabriela", "Mathieu", email = "calcita@gmx.li", role = c("aut", "cre"),                         comment = c(ORCID = "0000-0003-3965-9024")),
             person("Leticia", "Pineyro", email = "lpineyro@mides.gub.uy", role = "aut"))
Maintainer: Gabriela Mathieu <gmathieu@mides.gub.uy>
Description: R functions to estimate the TUS and AFAM population using the Encuesta Continua de Hogares (ECH) from Uruguay
at <http://www.ine.gub.uy/encuesta-continua-de-hogares1> conducted by the
Instituto Nacional de Estadistica (INE).
License: GPL-3
Encoding: UTF-8
ByteCompile: true
LazyData: true
```
